﻿using UnityEngine;
using System.Collections;

public class EngineStartButton : MonoBehaviour {

	public float timeToPress;
	public GameObject observer;
	public float minTimeBetweenPresses; // to prevent user pushing button twice in a second and end the game accidentally
	private bool buttonPressed;

	float buttonPress_TimeStamp;
	float buttonEndPress_TimeStamp;

	// Use this for initialization
	void Start () {
		buttonPressed = false;
		buttonEndPress_TimeStamp = float.MinValue;
	}



	void OnTriggerStay(Collider other) {

		if (Time.time - buttonEndPress_TimeStamp < minTimeBetweenPresses) {
			buttonPress_TimeStamp = Time.time;
		}
		//Debug.Log ("Time for press: " + (Time.time - buttonPress_TimeStamp)); // time in seconds
		//Debug.Log ("Time for next press: " + (Time.time - buttonEndPress_TimeStamp)); // time in seconds
		if (!buttonPressed && Time.time - buttonPress_TimeStamp >= timeToPress) {
			observer.SendMessage ("EngineButtonPressed");
			buttonPressed = true;
			buttonEndPress_TimeStamp = Time.time;
			Debug.Log ("Button pressed");
		}
	}

	void OnTriggerExit(Collider other){
		buttonPressed = false;
		
	}
}
