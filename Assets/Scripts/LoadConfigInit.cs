﻿using UnityEngine;
using System.Collections;

public class LoadConfigInit : MonoBehaviour {


	void Start () {
		using (System.IO.StreamReader file = new System.IO.StreamReader(Application.dataPath + "/../config.txt", System.Text.Encoding.UTF8))
		{
			HarvesterConfig.LoadConfig(file.ReadToEnd());
			
		}
		Application.LoadLevel ("Home");
	}

}
