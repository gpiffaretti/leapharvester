﻿using UnityEngine;
using System.Collections;

public class Wheel : MonoBehaviour {

	public GrabbingHand leftHand;
	public GrabbingHand rightHand;

	// user can move both hands independently, so its difficult to move the
	// steering wheel accordingly. We'll better use a main hand and respond to its movements
	public GrabbingHand mainHand; 

	private GrabbingHand.PinchState leftHandState;
	private GrabbingHand.PinchState rightHandState;

	public bool isLeftHandGrabbing;
	public bool isRightHandGrabbing;

	public Vector3 leftGrabPosition;
	public Vector3 rightGrabPosition;
	public Vector3 mainHandCurrentGrabPosition;

	public float maxRotationAngle;
	private float minAngleToCenter = 1;

	// Use this for initialization
	void Start () {
		leftHandState = GrabbingHand.PinchState.kReleased;
		rightHandState = GrabbingHand.PinchState.kReleased;
	}

	// returns value between [-1,1]
	public float GetInput(){
		Vector3 eulerAngles = transform.rotation.eulerAngles;
		if (eulerAngles.z > 180)
			return (eulerAngles.z - 360) / maxRotationAngle;
		else
			return eulerAngles.z / maxRotationAngle;

	}

	public bool IsGrabbed ()
	{
		return isLeftHandGrabbing || isRightHandGrabbing;
	}
	
	// Update is called once per frame
	void Update () {
		if (mainHand != null) {
			Vector3 mainGrabPosition = Vector3.zero;
			if (mainHand.Equals (leftHand)) {
				mainGrabPosition = leftGrabPosition;
			} else if (mainHand.Equals (rightHand)) {
				mainGrabPosition = rightGrabPosition;
			}
			Vector3 v1 = mainGrabPosition - transform.position;
			v1.z = 0;
			Vector3 v2 = mainHandCurrentGrabPosition - transform.position;
			v2.z = 0;
			float angle = Vector3.Angle (v1, v2);
			angle = Mathf.Clamp (angle, 0, maxRotationAngle);
			Vector3 cross = Vector3.Cross (v1, v2);
			if (transform.InverseTransformDirection (cross).z < 0)
				angle = -angle;
			Debug.DrawRay (transform.position, cross * 5);

			//Debug.Log("Update rotation: "+ angle);
			transform.rotation = Quaternion.Euler (0, 180, angle);
		} else { // return to default position
			Vector3 eulerAngles = transform.rotation.eulerAngles;
			float angle = eulerAngles.z > 180 ? eulerAngles.z - 360 : eulerAngles.z;
			if(Mathf.Abs(angle) < minAngleToCenter)
				transform.rotation = Quaternion.Euler (0, 180, 0);
			if(angle > 0){
				transform.Rotate(0, 0, angle*angle*-0.005f);
			}else if(angle < 0){
				transform.Rotate(0, 0, angle*angle*0.005f);
			}
		}

		//Debug.Log("Input: " + GetInput());
	}

	void OnTriggerEnter(Collider other) {
		if (rightHand != null && other.collider.transform.root.gameObject.Equals (rightHand.gameObject)) {
			isRightHandGrabbing = true;
			rightGrabPosition = other.transform.position;
			if(mainHand == null) mainHand = rightHand;
			Debug.Log ("Grabbed wheel with right hand");
			rightHandState = GrabbingHand.PinchState.kPinched;
		}
			
		
		if (leftHand != null && other.collider.transform.root.gameObject.Equals (leftHand.gameObject)) {
			isLeftHandGrabbing = true;
			leftGrabPosition = other.transform.position;
			if(mainHand == null) mainHand = leftHand;
			Debug.Log ("Grabbed wheel with left hand");
			leftHandState = GrabbingHand.PinchState.kPinched;
		}

	}

	void OnTriggerStay(Collider other) {
		// RIGHT HAND LOGIC
		if (rightHand != null && other.collider.transform.root.gameObject.Equals (rightHand.gameObject)) {
			if(rightHand.Equals(mainHand) && isRightHandGrabbing){
				//Debug.Log("right collider: " + other.gameObject.name);
				mainHandCurrentGrabPosition = other.transform.position;
			}
			/*GrabbingHand.PinchState newRightHandState = rightHand.GetPinchState();
			// if started grabbing
			if(rightHandState != GrabbingHand.PinchState.kPinched && newRightHandState == GrabbingHand.PinchState.kPinched){
				isRightHandGrabbing = true;
				rightGrabPosition = other.transform.position;
				if(mainHand == null) mainHand = rightHand;
				Debug.Log ("Grabbed wheel with right hand");
			}  
			// if stopped grabbing
			else if(rightHandState == GrabbingHand.PinchState.kPinched && newRightHandState != GrabbingHand.PinchState.kPinched){
				isRightHandGrabbing = false;
				if(mainHand != null && mainHand.Equals(rightHand)){
					if(isLeftHandGrabbing){
						mainHand =  leftHand;
						ResetHandGrabPosition(true);
					}else{
						mainHand = null;
					}
				} 
				Debug.Log ("Released wheel with right hand");
			}
			rightHandState = newRightHandState;*/
		}

		// LEFT HAND LOGIC
		if(leftHand != null && other.collider.transform.root.gameObject.Equals(leftHand.gameObject)){
			if(leftHand.Equals (mainHand) && isLeftHandGrabbing){
				//Debug.Log("left collider: " + other.gameObject.name);
				mainHandCurrentGrabPosition = other.transform.position;
			}
			/*GrabbingHand.PinchState newLeftHandState = leftHand.GetPinchState();
			// if started grabbing
			if(leftHandState != GrabbingHand.PinchState.kPinched && newLeftHandState == GrabbingHand.PinchState.kPinched){
				isLeftHandGrabbing = true;
				leftGrabPosition = other.transform.position;
				if(mainHand == null) mainHand = leftHand;
				Debug.Log("Grabbed wheel with left hand");
			}
			// if stopped grabbing
			else if(leftHandState == GrabbingHand.PinchState.kPinched && newLeftHandState != GrabbingHand.PinchState.kPinched){
				isLeftHandGrabbing = false;
				if(mainHand != null && mainHand.Equals(leftHand)){
					if(isRightHandGrabbing){
						mainHand =  rightHand;
						ResetHandGrabPosition(false);
					}else{
						mainHand = null;
					}
				} 
				Debug.Log("Released wheel with left hand");
			}
			leftHandState = newLeftHandState;*/
		}

	}

	void OnTriggerExit(Collider other) {
		if (rightHand == null)
			isRightHandGrabbing = false;
		if (leftHand == null)
			isLeftHandGrabbing = false;

		if (rightHand != null && other.collider.transform.root.gameObject.Equals (rightHand.gameObject)) {
			Debug.Log("Exit right hand");
			isRightHandGrabbing = false;
			rightHandState = GrabbingHand.PinchState.kReleased;
			if(isLeftHandGrabbing){
				mainHand = leftHand;
				ResetHandGrabPosition(true);
			}else{
				mainHand = null;
			}
		}

		if (leftHand != null && other.collider.transform.root.gameObject.Equals (leftHand.gameObject)) {
			Debug.Log("Exit left hand");
			isLeftHandGrabbing = false;
			leftHandState = GrabbingHand.PinchState.kReleased;
			if(isRightHandGrabbing){
				mainHand = rightHand;
				ResetHandGrabPosition(false);
			}else{
				mainHand = null;
			}
		}
			

	}

	private void ResetHandGrabPosition(bool left){
		if (left) {
			Transform t = leftHand.transform.FindChild("palm");
			leftGrabPosition = t.position;
		} else {
			Transform t = rightHand.transform.FindChild("palm");
			leftGrabPosition = t.position;
		}
	}

	public void DestroyLeftHand(){
		Debug.Log ("Left hand destroyed");
		if (mainHand != null && leftHand.gameObject.Equals (mainHand.gameObject))
			mainHand = null;
		leftHand = null;
	}

	public void DestroyRightHand(){
		Debug.Log ("Right hand destroyed");
		if (mainHand != null && rightHand.gameObject.Equals (mainHand.gameObject))
			mainHand = null;
		rightHand = null;
	}
}
