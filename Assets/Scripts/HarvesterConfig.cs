//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using SimpleJSON;
using UnityEngine;

public static class HarvesterConfig
{

	static private JSONNode rootNode;

	public static void LoadConfig(string fileText){
		rootNode = JSON.Parse (fileText);
		Debug.Log ("Loaded JSON config");
		Debug.Log (rootNode.ToString());
	}

	public static JSONArray GetArray(string category, string key){
		Debug.Log((rootNode [category] [key]).ToString ());
		return rootNode[category][key] as JSONArray;
	}

	public static int GetInt(string category, string key){
		Debug.Log((rootNode [category] [key]).ToString ());
		return rootNode[category][key].AsInt;
	}

	public static string GetString(string category, string key){
		Debug.Log((rootNode [category] [key]).ToString ());
		return rootNode[category][key].Value;
	}
}


