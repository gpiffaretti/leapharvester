﻿using UnityEngine;
using System.Collections;

public class Harvester : MonoBehaviour {

	public float maxSpeed;
	public float acceleration;
	public float fTurningSpeed;
	public Transform arado;
	public Terrain terrain;
	public bool arando;
	public Wheel steeringWheel;
	public ParticleSystem particles;
	public AudioClip engineStop;
	public float minPitch;
	public float maxPitch;

	private float currentSpeed;
	private bool throttle;
	private TerrainData terrainData;
	private Vector3 normalizedPosition;
	private int[,] detailMap;
	public Rotate aradoRotacion;

	// Use this for initialization
	void Start () {
		terrainData = terrain.terrainData;
		normalizedPosition = new Vector3 ();

	}

	public void EnableArado(bool enabled){
		particles.enableEmission = enabled;
		arando = enabled;
		aradoRotacion.enabled = enabled;
	}

	public bool isArando(){
		return arando;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.A)) {
			EnableArado(!arando);
		}

		float inputH;

		throttle = steeringWheel.IsGrabbed () || Input.GetKey(KeyCode.UpArrow);
		//Debug.Log(throttle ? "accelerating" : "breaking");
		inputH = steeringWheel.GetInput ();
		float keyboardInput = Input.GetAxis("Horizontal");
		if (keyboardInput != 0)
			inputH = keyboardInput;
		//Debug.Log ("Harvester steer input: " + inputH);
		//Debug.Log ("Harvester turning speed: " + fTurningSpeed * inputH);

		// update harvester speed
		float accelerationDirection = throttle ? 1 : -1;

		currentSpeed += acceleration * accelerationDirection * Time.deltaTime;
		currentSpeed = Mathf.Clamp (currentSpeed, 0, maxSpeed);
		transform.Translate (0,0,currentSpeed * Time.deltaTime);

		float pitchInterpolationValue = currentSpeed / maxSpeed;
		audio.pitch = Mathf.Lerp (minPitch, maxPitch, pitchInterpolationValue);

		if(currentSpeed > 0)
			transform.Rotate(Vector3.up, fTurningSpeed * inputH * Time.deltaTime);

		if (arando) {
			Vector3 positionInTerrain = arado.position - terrain.GetPosition();
			normalizedPosition.x = positionInTerrain.x / terrainData.size.x;
			normalizedPosition.y = positionInTerrain.y;
			normalizedPosition.z = positionInTerrain.z / terrainData.size.z;
			
			//Debug.Log (normalizedPosition);
			
			int detailX = (int)(normalizedPosition.x * terrainData.detailResolution);
			int detailY = (int)(normalizedPosition.z * terrainData.detailResolution);
			int mapWidth = 30;
			int mapHeight = 40;
			detailMap = terrainData.GetDetailLayer (
				detailX-mapWidth/2,
				detailY-mapHeight/2,
				mapWidth, mapHeight, 0);
			
			int hipotenusa2 = (mapWidth/2) * (mapWidth/2);
			
			for(int i = 0; i < detailMap.GetLength(0); i++){
				for (int j = 0; j < detailMap.GetLength(1); j++) {
					int cateto1 = (i - mapWidth/2);
					int cateto2 = (j - mapHeight/2);
					if(cateto1*cateto1 + cateto2*cateto2 < hipotenusa2){
						int value = 0;//(int) (Mathf.Clamp01((i*i + j*j) / (float)hipotenusa2));
						//if(value < detailMap[i,j]) 
						detailMap[i,j] = value;
					}


				}
			}
			
			// Assign the modified map back.
			terrainData.SetDetailLayer(detailX-mapWidth/2, detailY-mapHeight/2, 0, detailMap);
		}

	}

	public void StartHarvester(){
		enabled = true;
		audio.enabled = true;
		particles.gameObject.SetActive (true);
	}

	public void StopHarvester(){
		audio.Stop ();
		audio.clip = engineStop;
		audio.loop = false;
		audio.Play ();
		Invoke ("SelfDisable", 2f);
		Renderer[] renderers = GetComponentsInChildren<Renderer> ();
		foreach (var renderer in renderers) {
			renderer.enabled = false;
		}
	}

	void SelfDisable(){
		gameObject.SetActive (false);
	}
}
