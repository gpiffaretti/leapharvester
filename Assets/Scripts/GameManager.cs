﻿using UnityEngine;
using System.Collections;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Threading;
using System;
using System.Collections.Generic;
using SimpleJSON;

public class GameManager : MonoBehaviour {

	public Harvester harvester;
	public Lever lever;
	public CameraScreenshot cameraScreenshot;
	public GameObject handController;
	public GameObject handControllerGUI;

	public UIPanel panelInstructions;
	public UIWidget widgetInstructions;

	public UIPanel panelGameEnded;
	public UITexture screenshotPreview;
	public UISprite whiteOverlayEnd;

	private Terrain terrain;
	private bool gameEnded = false;
	private bool gameStarted = false;

	private string userEmail;
	private string userPhone;
	private string screenshotFileName;
	private string screenshotPath;
	private string footerPath;

	// Use this for initialization
	void Start () {
		//Camera.main.GetComponent<Animator> ().Play ("CameraIntro");
		//Invoke ("EnableHarvester", 1);
		terrain = FindObjectOfType<Terrain> ();
		FillTerrain ();
		footerPath = "footerEmail.png";
	}

	void EnableHarvester() {
		harvester.enabled = true;
	}

	public void FillTerrain(){
		Debug.Log("Started filling terrain with grass");
		TerrainData terrainData = terrain.terrainData;
		int[,] detailMap = terrain.terrainData.GetDetailLayer (
			0, 0,
			terrainData.detailWidth, terrainData.detailHeight, 0);

		
		for(int i = 0; i < detailMap.GetLength(0); i++){
			for (int j = 0; j < detailMap.GetLength(1); j++) {
				detailMap[i,j] = 15;

			}
		}
		Debug.Log("Terrain filled");

		terrainData.SetDetailLayer (0, 0, 0, detailMap);
	}

	public void EngineButtonPressed(){
		// game starts
		if (!gameStarted) {
			StartGame();
		} else {
			gameEnded = true;
			cameraScreenshot.TakeHiResShot();
		}

	}

	private void StartGame(){
		gameStarted = true;
		audio.Play ();
		Invoke ("HideInstructionsAndEnableHarvester", 3f);
	}

	private void HideInstructionsAndEnableHarvester(){
		harvester.StartHarvester ();
		lever.GameStarted();
		widgetInstructions.GetComponent<TweenAlpha>().enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Return)) {
			if(!gameEnded){
				EngineButtonPressed();
			}else{
				Application.LoadLevel("Home");
			}
			
		}

		/*if (Input.GetKeyDown (KeyCode.K)) {
			Application.CaptureScreenshot("screenshotHarvester", 1);
			Debug.Log("Screenshot captured");
		}*/
	}

	public void ReturnToHome(){
		Application.LoadLevel("Home");
	}

	void SendEmail(){
		try{
			// mail a nuestra direccion
			MailMessage mailPertilco = new MailMessage();
			mailPertilco.Subject = "Captura de pantalla - " + userEmail;
			mailPertilco.From = new MailAddress("contacto@pertilco.com.uy");
			JSONArray destinatarios = HarvesterConfig.GetArray("configEmail", "destinatarios");
		//	Debug.Log("Destinatarios: " + destinatarios.ToString());
			for (int i = 0; i < destinatarios.Count; i++) {
				mailPertilco.To.Add(destinatarios[i]);
				Debug.Log("Added mail recipient: "+destinatarios[i]);
			}

			mailPertilco.Body = "Capura de pantalla del usuario: \n" + userEmail + "\n" + userPhone;

			// mail al usuario
			MailMessage mailUsuario = new MailMessage();
			
			mailUsuario.From = new MailAddress("contacto@pertilco.com.uy");
			mailUsuario.To.Add(userEmail);
			mailUsuario.Subject = "Pertilco SA - gracias por jugar con nosotros!";
			mailUsuario.IsBodyHtml = true;
			mailUsuario.Body = GetEmailBodyHTML() + GetEmailFooter();

			Debug.Log ("looking for attachment in: " + screenshotPath);
			
			System.Net.Mail.Attachment attachment1 = new System.Net.Mail.Attachment(screenshotPath);
			mailPertilco.Attachments.Add(attachment1);
			System.Net.Mail.Attachment attachment2 = new System.Net.Mail.Attachment(screenshotPath);
			mailUsuario.Attachments.Add(attachment2);
			
			SmtpClient smtpServer = new SmtpClient(HarvesterConfig.GetString("configEmail", "smtpServer"));
			smtpServer.Port = HarvesterConfig.GetInt("configEmail", "puerto");
			smtpServer.Credentials = new System.Net.NetworkCredential(HarvesterConfig.GetString("configEmail", "cuenta"), HarvesterConfig.GetString("configEmail", "password")) as ICredentialsByHost;
			//Debug.Log("created credentials");
			smtpServer.EnableSsl = true;
			ServicePointManager.ServerCertificateValidationCallback = 
				delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) 
			{ return true; };
			smtpServer.Send(mailPertilco);
			smtpServer.Send(mailUsuario);
			Debug.Log("Emails sent");
		} catch(Exception e){
			Debug.Log("Error sending mail: " + e.Message);
		}


	}

	// callback from camera screenshot. Use this to end game
	void ScreenshotTaken(Dictionary<string, string> values){
		screenshotFileName = values["filename"];
		StartCoroutine(WaitForImageLoaded(values["fullPath"]));

	}

	IEnumerator WaitForImageLoaded(string filename){
		string path = "file://" + Application.dataPath + "/../" + filename;
		Debug.Log (path);

		Debug.Log("Waiting for screenshot to load");
		//path = "http://thejointblog.com/wp-content/uploads/2013/07/uruguay1.jpg";
		WWW file = new WWW (path);

		// wait until the download is done
		yield return file;
		Debug.Log("Screenshot loaded in memory");
		Texture2D screenshot = null;

		
		screenshotPreview.mainTexture = file.texture;
		ShowScreenshot (filename);
	}

	void ShowScreenshot (string path){
		panelGameEnded.gameObject.SetActive(true);
		TweenScale scaleTween = screenshotPreview.GetComponent<TweenScale> ();
		scaleTween.ResetToBeginning (); scaleTween.PlayForward ();
		TweenAlpha alphaTween = whiteOverlayEnd.GetComponent<TweenAlpha> ();
		alphaTween.ResetToBeginning (); alphaTween.PlayForward ();
		Invoke ("HideTerrainAndHarvester", 0.2f);
		screenshotPath = path;
		userEmail = UserData.Instance.email;
		userPhone = UserData.Instance.phone;
		SaveGameSessionToTextFile ();
		Thread oThread = new Thread(new ThreadStart(SendEmail));
		oThread.Start ();

		//Invoke ("SendEmail", 0f);
	}

	void HideTerrainAndHarvester(){
		harvester.StopHarvester ();
		handController.gameObject.SetActive (false);
		handControllerGUI.gameObject.SetActive (true);
		terrain.gameObject.SetActive(false);
	}

	string GetEmailBodyPlain ()
	{
		return "Gracias nuevamente por jugar junto a Pertilco, representante de Case IH Agriculture en Uruguay.\n\n"
			+ "Adjunta te enviamos la imagen de la huella que dejaste en la ExpoActiva 2015 y te invitamos a compartirla con tus amigos y familia.\n\n"
				+ "Te recordamos además que estás participando en el sorteo de un par de botas, que se realizará el sábado 21 a las 16 hs. De resultar ganador te contactaremos telefónicamente.\n\n"
				+ "Quedamos a las órdenes por cualquier consulta. Saludos,\n\n"
				+ "Pertilco S.A.";
	}

	private string GetEmailBodyHTML(){
		return "<p>Gracias nuevamente por jugar junto a Pertilco, representante de Case IH Agriculture en Uruguay.</p>"
			+ "<p>Adjunta te enviamos la imagen de la huella que dejaste en la ExpoActiva 2015 y te invitamos a compartirla con tus amigos y familia.</p>"
				+ "<p>Te recordamos además que estás participando en el sorteo de un par de botas, que se realizará el sábado 21 a las 16 hs. De resultar ganador te contactaremos telefónicamente.</p>"
				+ "<p>Quedamos a las órdenes por cualquier consulta. Saludos,</p>"
				+ "<p>Pertilco S.A.</p>";
	}

	private string GetEmailFooter(){
		//<img src=cid:companylogo>
		//return "<img src=cid:companylogo alt='footer' width='452' height='102'>";
		//
		return "<img src='http://s4.postimg.org/k5419um59/footer_Email.png' alt='footer' width='452' height='102'>";
	}

	private void SaveGameSessionToTextFile(){
		using (System.IO.StreamWriter file = new System.IO.StreamWriter(Application.dataPath + "/../datosUsuarios.txt", true))
		{
			file.WriteLine(userEmail + "##" + userPhone + "##" + screenshotFileName);

		}
	}
}
