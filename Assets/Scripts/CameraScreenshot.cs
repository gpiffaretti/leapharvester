﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraScreenshot : MonoBehaviour {

	public GameObject observer;
	public Texture2D caseLogo;
	public Texture2D pertilcoLogo;

	public int resWidth; 
	public int resHeight;

	private bool takeHiResShot = false;
	private static string path = "screenshots/";
	private static string filenameFormat = "screen_{0}x{1}_{2}.jpg";
	
	public static string ScreenShotName(int width, int height) {
		return string.Format(filenameFormat,
		                     width, height, 
		                     System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
	}

	void Start(){
		resWidth = (int) Camera.main.pixelWidth;
		resHeight = (int) Camera.main.pixelHeight;
	}
	
	public void TakeHiResShot() {
		takeHiResShot = true;
	}
	
	void Update() {
		//takeHiResShot |= Input.GetKeyDown(KeyCode.S);
		if (takeHiResShot) {
			takeHiResShot = false;
			try{
				RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
				camera.targetTexture = rt;
				Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
				camera.Render();

				RenderTexture.active = rt;
				screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
				camera.targetTexture = null;
				RenderTexture.active = null; // JC: added to avoid errors
				Destroy(rt);
			//	Texture2D screenshotWatermark = AddWatermarks(screenShot);
				byte[] bytes = screenShot.EncodeToJPG();
				string filename = ScreenShotName(resWidth, resHeight);
				System.IO.File.WriteAllBytes(path+filename, bytes);
				Debug.Log(string.Format("Took screenshot to: {0}", path+filename));
				observer.SendMessage("ScreenshotTaken", new Dictionary<string,string>(){{"fullPath",path+filename}, {"filename", filename}} );
			}catch(UnityException e){
				Debug.Log("Error while taking screenshot: "+e.Message);
			}

		}
	}

	private Texture2D AddWatermarks(Texture2D background)
	{
		// Create a new writable texture.
		Texture2D result = new Texture2D(background.width, background.height);
		
		// Draw watermark at bottom right corner.
		int startX = background.width - caseLogo.width;
		int startY = background.height - caseLogo.height;
		
		for (int x = 0; x < background.width; x++) {
			for (int y = 0; y < background.height; y++) {
				Color bgColor = background.GetPixel(x, y);
				Color wmColor = new Color(0, 0, 0, 0);
				
				// Change this test if no longer drawing at the bottom right corner.
				if (x >= startX && y >= startY) {
					wmColor = caseLogo.GetPixel(x, y);
				}
				
				// Alpha-blend background and watermark color.
				Color blended = bgColor * (1.0f - wmColor.a) + wmColor * (new Color(0,0,0,0.6f));
				blended.a = 1.0f;
				
				result.SetPixel(x, y, blended);
			}
		}
		
		result.Apply();
		return result;
	}
}
