﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class MenuManager : MonoBehaviour {

	private UserData userData;
	public UIInput emailInput;
	public UIInput phoneInput;

	public string regexMail;
	public string regexPhone;

	// Use this for initialization
	void Start () {
		userData = UserData.Instance;
		emailInput.isSelected = true;
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Tab)) {
			if(emailInput.isSelected){
				phoneInput.isSelected = true;
			}else {
				emailInput.isSelected = true;
			}
		
		}
		if (Input.GetKeyDown(KeyCode.Return)) {
			AttemptStartGame();
		}
	}
	
	public void AttemptStartGame(){
		bool matchMail = Regex.IsMatch(emailInput.value, regexMail);
		bool matchPhone = Regex.IsMatch (phoneInput.value, regexPhone);
		Debug.Log (matchMail ? "Mail correct" : "Mail incorrect");
		Debug.Log (matchPhone ? "Phone correct" : "Phone incorrect");
		if(matchMail && matchPhone){
			// check email
			userData.email = emailInput.value;
			userData.phone = phoneInput.value;
			// if everything ok
			StartGame();
		}


	}

	void PlayEngineStart(){
		audio.Play ();
	}

	void StartGame(){
		Application.LoadLevel("Harvester");
	}
}
