﻿using UnityEngine;
using System.Collections;

public class LeapGUIButton : MonoBehaviour {

	public GameObject target;

	// Use this for initialization
	void Start () {
	
	}


	void OnTriggerEnter(Collider other) {
		target.SendMessage("OnPress", true);
	}

	void OnTriggerStay(Collider other) {

	}

	void OnTriggerExit(Collider other){
		UIInput input = target.GetComponent<UIInput> ();
		if(input != null) input.isSelected = true;
		target.SendMessage("OnSelect", true);
		target.SendMessage("OnPress", false);
		target.SendMessage("OnClick");
	}
}
