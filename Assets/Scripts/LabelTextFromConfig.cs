﻿using UnityEngine;
using System.Collections;

public class LabelTextFromConfig : MonoBehaviour {

	public string key;

	// Use this for initialization
	void Start () {
		UILabel label = GetComponent<UILabel> ();
		if(label != null)
			label.text = HarvesterConfig.GetString ("textosMenus", key);

		UIInput input = GetComponent<UIInput> ();
		if (input != null) {
			input.value = HarvesterConfig.GetString ("textosMenus", key);
		}
	}

}
