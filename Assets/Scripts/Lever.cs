﻿using UnityEngine;
using System.Collections;

public class Lever : MonoBehaviour {

	public Harvester harvester;
	public float maxDistanceFromCenter;

	private bool leverActive;


	public bool gameStarted;
	public float timeToActivate;

	// Use this for initialization
	void Start () {
		leverActive = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void GameStarted(){
		gameStarted = true;
		if(gameStarted) harvester.EnableArado(leverActive);
	}

	void enableArado(){
		leverActive = true;
		Debug.Log ("activar arado");
		animation.CrossFade ("right");
		if (gameStarted) harvester.EnableArado (true);

	}

	void disableArado(){
		Debug.Log("desactivar arado");
		leverActive = false;
		animation.CrossFade("left");
		if(gameStarted) harvester.EnableArado(false);
	}

	void OnTriggerStay(Collider other) {

		Vector3 handLocalPosition = transform.InverseTransformPoint(other.transform.position);


		//Debug.Log ("Tocando la palanca!!! " + handLocalPosition.z);
		if (handLocalPosition.z <= 0 && !leverActive) { // activar arado
			CancelInvoke("disableArado");
			if(!IsInvoking("enableArado")){
				Invoke("enableArado", timeToActivate);
			}
				
		} else if(handLocalPosition.z > 0  && leverActive) { // desactivar arado
			CancelInvoke("enableArado");
			if(!IsInvoking("disableArado")){
				Invoke("disableArado", timeToActivate);
			}
		}


	}

	void OnTriggerExit(Collider other) {
		CancelInvoke ("enableArado");
		CancelInvoke ("disableArado");
	}
}
